// DSPIC30F6012A Configuration Bit Settings

// 'C' source line config statements

// FOSC
#pragma config FOSFPR = HS2_PLL8		// Oscillator (HS2 w/PLL 8x)
#pragma config FCKSMEN = CSW_FSCM_OFF	// Clock Switching and Monitor (Sw Disabled, Mon Disabled)

// FWDT
#pragma config FWPSB = WDTPSB_16		// WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512		// WDT Prescaler A (1:512)
#pragma config WDT = WDT_OFF			// Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_OFF			// POR Timer Value (Timer Disabled)
#pragma config BODENV = NONE			// Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_OFF			// PBOR Enable (Disabled)
#pragma config MCLRE = MCLR_EN			// Master Clear Enable (Enabled)

// FBS
#pragma config BWRP = WR_PROTECT_BOOT_OFF// Boot Segment Program Memory Write Protect (Boot Segment Program Memory may be written)
#pragma config BSS = NO_BOOT_CODE		// Boot Segment Program Flash Memory Code Protection (No Boot Segment)
#pragma config EBS = NO_BOOT_EEPROM		// Boot Segment Data EEPROM Protection (No Boot EEPROM)
#pragma config RBS = NO_BOOT_RAM		// Boot Segment Data RAM Protection (No Boot RAM)

// FSS
#pragma config SWRP = WR_PROT_SEC_OFF	// Secure Segment Program Write Protect (Disabled)
#pragma config SSS = NO_SEC_CODE		// Secure Segment Program Flash Memory Code Protection (No Secure Segment)
#pragma config ESS = NO_SEC_EEPROM		// Secure Segment Data EEPROM Protection (No Segment Data EEPROM)
#pragma config RSS = NO_SEC_RAM			// Secure Segment Data RAM Protection (No Secure RAM)

// FGS
#pragma config GWRP = GWRP_OFF			// General Code Segment Write Protect (Disabled)
#pragma config GCP = GSS_OFF			// General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD			// Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include "lib_pic30f/timing.h"
#include <libpic30.h>
#include "config.h"
// NOTE: Always include timing.h

Status_car car;
Status_board board;

void timer1_callback(){

    //send status every 1s
	if(((board.time_stamp)%1000) == 0){
		board.send_status = 1;
	}
    //get the current value every 6ms (100/16=6)
	if (board.time_stamp%6 == 0) {
		get_current();
	}
    //send current status every 100ms
	if (board.time_stamp%100 == 0) {
		send_current_status();
		set_brake_light();
	}

	if (board.time_stamp%400 == 0) {
		if (OC4RS == DRS_MAX_SIG*5/6){
			OC4RS = DRS_MAX_SIG/3;
		}
		else {
			OC4RS = DRS_MAX_SIG*5/6;
		}
	}
	board.time_stamp++;
}

void timer2_callback(){
	/* Start buzzing */
	if (car.RTD_buzz == 1 && board.RTD_buzz_count == 0) {
		BUZZ_SIG = 1;
		board.RTD_buzz_count++;
	}
	/* Keep buzzing */
	else if (car.RTD_buzz == 1 && board.RTD_buzz_count < 250) {
		board.RTD_buzz_count++;
	}
	/* End buzzing */
	else if (car.RTD_buzz == 1) {
		BUZZ_SIG = 0;
		board.RTD_buzz_count = 0;
		car.RTD_buzz = 0;
		send_rtd_message();
	}
		
	return;
}

int main(void)
{
	ADC_parameters parameters;
	config_can2();
	config_pins();
	config_timer1(1,6);             //sends status
	config_timer2(10,7);            //executes rtd sequence - KEEP PRIORITY HIGH
	config_timer3(7,5);            //timer for the pwm
	config_adc_parameters(&parameters);
	config_adc(parameters);
	config_pwm_drs();
	set_default_signals();

	CANdata reset_message = make_reset_msg(DEVICE_ID_DCU,RCON);
	send_can2(&reset_message);


	//initialization counters
	board.RTD_buzz_count = 0;
	board.current_step = 0; 

	//TO DO: call new reset function
	//reset_flag();
	
	FAN_SIG = 0;
	__delay_ms(1000);

	OC4RS = DRS_MAX_SIG/3;
	//FAN_SIG = 0;
	BLUE_SIG = 1;
	BL_SIG = 1;
	//OC4RS = DRS_MIN_SIG;
	while (1) {

		PUMP1_SIG = 1;
		PUMP2_SIG = 1;
		send_can2_buffer();
		receive_can();
		if(board.send_status == 1){
			send_status();
		}
		Idle();
	}

	return 0;
}
