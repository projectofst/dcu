#include "config.h"

//called in the main while and selects what actions to take depending on the received message
//redue this one
void receive_can(){
    CANdata *message;  
    COMMON_MSG_RTD_ON rtd_message; 
    COMMON_MSG_RTD_OFF rtd_off_message;
    INTERFACE_MSG_DCU_TOGGLE dcu_toggle_message;
    INTERFACE_MSG_DEBUG_TOGGLE debug_message;
    TE_MESSAGE te_message;

    message = pop_can2();
    if(message == NULL)
        return;       

    /*set rtd sequence*/
    if(message->msg_id == CMD_ID_COMMON_RTD_ON){
        parse_can_common_RTD(*message,&rtd_message);
        set_RTD_sequence(rtd_message); 
    }

    /*rtd off sequence */
    if(message->msg_id == CMD_ID_COMMON_RTD_OFF){
        parse_can_common_RTD_OFF(*message, &rtd_off_message);
        deactivate_RTD_mode(rtd_off_message);
    }
    /*interface toggle interaction*/
    if(message->dev_id == DEVICE_ID_INTERFACE){
        if(message->msg_id == CMD_ID_INTERFACE_DCU_TOGGLE){
            parse_can_message_dcu_toggle(message->data, &dcu_toggle_message);
            toggle_signals(dcu_toggle_message);
        }
        if(message->msg_id == CMD_ID_INTERFACE_DEBUG_TOGGLE){
            parse_can_message_debug_toggle(message->data[0],&debug_message);
            set_bebug_mode(debug_message);
        }
    }
    /*TE message to get the BPS pressure*/
    if ((message->msg_id == MSG_ID_TE_MAIN) && (message->dev_id == DEVICE_ID_TE)){
        parse_te_main_message(message->data, &te_message);
		get_bps_pressure(te_message);
		if (te_message.status.Implausibility_APPS_BPS) {
			BLUE_SIG = 1;
		}
		else {
			BLUE_SIG = 0;
		}
    }
}

//message containig the dcu status
void send_status(){

    CANdata message_status;

    unsigned int ntc1;
    unsigned int ntc2;

    ntc1 = (check_temp(0)) >> 8;
    ntc2 = (check_temp(1)) & 0xFF00;

    message_status.dev_id = DEVICE_ID_DCU;         
    message_status.msg_id = MSG_ID_DCU_STATUS;
    message_status.dlc = 6;
    message_status.data[0] = check_detections();   /*DET               */
    message_status.data[1] = check_signals();      /*signals ????????  */
    message_status.data[2] = ntc1 | ntc2;        /*NTC1 value - AN0  */


    write_to_can2_buffer(message_status, 0);

    board.send_status = 0;

    return;
}

//message containing the HV/LV current values
void send_current_status(){
    CANdata message1;

    message1.dev_id = DEVICE_ID_DCU;
    message1.msg_id =  MSG_ID_DCU_CURRENT;
    message1.dlc = 4;
    message1.data[0] = convert_LV_current(get_average(car.LV_current));
    message1.data[1] = get_average(car.HV_current);

    write_to_can2_buffer(message1,0);
    return;
}

//message containing the next step of the rtd sequence
void send_rtd_message(){
    CANdata message;
    message.dev_id = DEVICE_ID_DCU;
    message.msg_id = CMD_ID_COMMON_RTD_ON;
    message.dlc = 2;
    message.data[0] = RTD_STEP_DCU;
    write_to_can2_buffer(message,1);
    return;
}

//message for trap handling
void send_trap_message(TrapType type){
    CANdata message;

    message.dev_id = DEVICE_ID_DCU;
    message.msg_id = MSG_ID_COMMON_TRAP;
    message.dlc = 2;
    message.data[0] = type;

    write_to_can2_buffer(message, 0);
    return;
}
