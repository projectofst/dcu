#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <xc.h>
#include "lib_pic30f/timing.h"
#include "lib_pic30f/can.h"
#include "lib_pic30f/timer.h"
#include "lib_pic30f/ADC.h"
#include "lib_pic30f/trap.h"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/DCU_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/TE_CAN.h"



/* IO definitions */
#define TRIS_NTC1_SIG               TRISBbits.TRISB0
#define TRIS_NTC2_SIG               TRISBbits.TRISB1 
#define TRIS_HV_CS                  TRISBbits.TRISB2 
#define TRIS_LV_CS                  TRISBbits.TRISB3 
#define TRIS_AN1					TRISBbits.TRISB4
#define TRIS_AN2					TRISBbits.TRISB5
#define TRIS_DET_SC_BSPD_BAT	    TRISBbits.TRISB8
#define TRIS_DET_SC_MH_FRONT	    TRISBbits.TRISB9
#define TRIS_DET_SC_FRONT_BSPD	    TRISBbits.TRISB10
#define TRIS_DET_VCC_BL		 	    TRISBbits.TRISB11
#define TRIS_DET_VCC_CAN_E	 	    TRISBbits.TRISB12
#define TRIS_DET_VCC_DRS  	 	    TRISBbits.TRISB13
#define TRIS_DET_VCC_CUB		 	TRISBbits.TRISB14
#define TRIS_DET_VCC_FANS	 	    TRISBbits.TRISB15

#define TRIS_GPIO2					TRISCbits.TRISC1
#define TRIS_BLUE_SIG               TRISCbits.TRISC2
#define TRIS_SC_SWITCH              TRISCbits.TRISC13
#define TRIS_PUMP1_SIG              TRISCbits.TRISC14

#define TRIS_FAN_SIG                TRISDbits.TRISD0 
#define TRIS_PUMP2_SIG              TRISDbits.TRISD1
#define TRIS_DCDC_SWITCH            TRISDbits.TRISD2
#define TRIS_DRS_SIG                TRISDbits.TRISD3
#define TRIS_BL_SIG                 TRISDbits.TRISD4
#define TRIS_BUZZ_SIG               TRISDbits.TRISD5
#define TRIS_DET_VCC_CUA            TRISDbits.TRISD8
#define TRIS_DET_VCC_TSAL 		    TRISDbits.TRISD9
#define TRIS_DET_VCC_FANS_AMS 	    TRISDbits.TRISD10
#define TRIS_DET_VCC_AMS            TRISDbits.TRISD11

#define TRIS_CUA_SIG                TRISFbits.TRISF2
#define TRIS_DET_SC_ORIGIN          TRISFbits.TRISF3
#define TRIS_DET_VCC_PUMPS          TRISFbits.TRISF4
#define TRIS_DET_VCC_EM             TRISFbits.TRISF5
#define TRIS_CUB_SIG                TRISFbits.TRISF6

#define TRIS_GPIO1					TRISGbits.TRISG15


/* Input definitions */
#define DET_SC_BSPD_BAT	            PORTBbits.RB8
#define DET_SC_MH_FRONT	            PORTBbits.RB9
#define DET_SC_FRONT_BSPD	        PORTBbits.RB10
#define DET_VCC_BL		 	        PORTBbits.RB11
#define DET_VCC_CAN_E	 	        PORTBbits.RB12
#define DET_VCC_DRS  	 	        PORTBbits.RB13
#define DET_VCC_CUB		 	        PORTBbits.RB14
#define DET_VCC_FANS	 	        PORTBbits.RB15

#define GPIO2						LATCbits.LATC1
#define BLUE_SIG                    LATCbits.LATC2
#define SC_SWITCH                   LATCbits.LATC13
#define PUMP1_SIG                   LATCbits.LATC14

#define FAN_SIG                     LATDbits.LATD0 
#define PUMP2_SIG                   LATDbits.LATD1
#define DCDC_SWITCH                 LATDbits.LATD2
#define BL_SIG                      LATDbits.LATD4
#define BUZZ_SIG                    LATDbits.LATD5

#define DET_VCC_CUA		 	        PORTDbits.RD8
#define DET_VCC_TSAL 		        PORTDbits.RD9
#define DET_VCC_FANS_AMS 	        PORTDbits.RD10
#define DET_VCC_AMS                 PORTDbits.RD11

#define CUA_SIG                     LATFbits.LATF2
#define DET_SC_ORIGIN               PORTFbits.RF3
#define DET_VCC_PUMPS               PORTFbits.RF4
#define DET_VCC_EM                  PORTFbits.RF5
#define CUB_SIG                     LATFbits.LATF6

/*Current sensor related values*/

#define VOE                         2500
#define IPN                         10
#define ADC_MAX_VALUE               4095 /*check this*/

/*drs related values*/

#define DRS_PERIOD                  (20*M_SEC)/256
#define DRS_MAX_SIG                 (M_SEC)*5/512
#define DRS_MIN_SIG                 (M_SEC)/512
#define DRS_RANGE                   100

/*BPS related values*/

#define BPS_PRESSURE_MAX            1000

typedef struct{
    bool RTD_buzz;
    bool RTD_mode;
    bool TS_ON;
    long int LV_current[16];
    long int HV_current[16];
    unsigned int DRS_sig;
    unsigned int DRS_max_value;
    unsigned int DRS_min_value;
    unsigned int bps_pressure;
	unsigned int apps;
}Status_car;

typedef struct{
    unsigned int time_stamp;
    unsigned int current_step;
    unsigned int RTD_buzz_count;
    bool debug_mode;
    bool send_status;
}Status_board;

extern Status_car car;
extern Status_board board;

//config.c
void config_pins();
void set_default_signals();
void config_adc_parameters(ADC_parameters *parameters);
void config_pwm_drs();

//communication.c
void receive_can();
void send_status();
void send_current_status();
void send_rtd_message();
void send_trap_message(TrapType type);

//actions.c
void set_RTD_sequence(COMMON_MSG_RTD_ON rtd_message);
void deactivate_RTD_mode(COMMON_MSG_RTD_OFF rtd_off_message);
void toggle_signals(INTERFACE_MSG_DCU_TOGGLE dcu_toggle_message);
void set_bebug_mode(INTERFACE_MSG_DEBUG_TOGGLE debug_message);
long convert_LV_current (unsigned int adc_value);
void get_current();
unsigned int get_average(int value[16]);
unsigned int check_detections();
unsigned int check_signals();
unsigned int check_temp(unsigned int pin);
void set_drs_duty_cycle();
void get_bps_pressure(TE_MESSAGE te_message);
void set_brake_light();

#endif
