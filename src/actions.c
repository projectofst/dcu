#include "config.h"
#include <libpic30.h>

//do the rtd sequence
void set_RTD_sequence(COMMON_MSG_RTD_ON rtd_message){
    //star rtd sequence by starting the buzzer
    if((rtd_message.step == RTD_STEP_INV_OK) && (car.RTD_mode == 0)){
        car.RTD_buzz = 1;
    }
    //ending rtd sequence by seting the rtd mode in the car struct
    if(rtd_message.step == RTD_STEP_INV_GO){  
        car.RTD_mode = 1;
    }
    return;
}

//checks if RTD mode is now off
void deactivate_RTD_mode(COMMON_MSG_RTD_OFF rtd_off_message){
    if(rtd_off_message.step == RTD_INV_OFF || rtd_off_message.step == RTD_INV_CONFIRM){
        car.RTD_mode = 0;
    }
    return;
}

//toggle the signals when required by the interface
//change for new messages
void toggle_signals(INTERFACE_MSG_DCU_TOGGLE dcu_toggle_message){
    switch(dcu_toggle_message.code){
        case DCU_FANS:
            FAN_SIG = ~FAN_SIG;
            break;
        case DCU_PUMPS1:
            PUMP1_SIG= ~PUMP1_SIG;
            break;
        case DCU_PUMPS2:
            PUMP2_SIG= ~PUMP2_SIG;
            break;
        case DCU_CUA:
            CUA_SIG= ~CUA_SIG;
            break;
        case DCU_CUB:
            CUB_SIG= ~CUB_SIG;
            break;
        case DCU_SC:
            SC_SWITCH= ~SC_SWITCH;
            break;
        case DCU_BLUE_TSAL:
            BLUE_SIG= ~BLUE_SIG;
            break;
        case DCU_DCDC_DRS:
            DCDC_SWITCH= ~DCDC_SWITCH;
            break;
    }
    
    return;
}

//toggle debug mode as required by theinterface
//not doing anything with this mode for now
void set_bebug_mode(INTERFACE_MSG_DEBUG_TOGGLE debug_message){
    if(debug_message.device == DEBUG_TOGGLE_DCU){
        board.debug_mode = !(board.debug_mode);
    }
    return;
}

//converts adc value in to a current value in mA
//used for the LV current sensor 
long convert_LV_current (unsigned int adc_value){
    long Vout;
    long Ip = 0;

    /*convert adc_value to voltage*/
    Vout = (5000UL*adc_value)/4095;
    /*convert output voltage to measured current*/
    if(Vout>=VOE){      /*positive current*/
        Ip = (((Vout-VOE)*IPN)*16)/10;

    }
    if(Vout<VOE){       /*negative current*/
        Ip = ((-1*(Vout-VOE)*IPN)*16)/10;
    }

    return Ip;

}

//gets the current values mesured by the sensor and converted in the adc and then converted by the respective fuctions
//missing fuction for the HV sensor
void get_current(){
    /*get LV_current*/
    select_input_pin(3);          /*AN3 pin -> LV_current*/
    start_samp();
    wait_for_done();
    car.LV_current[board.current_step] = get_adc_value(0);
    /*get HV_current*/
    select_input_pin(2);          /*AN2 pin -> HV_current*/
    start_samp();
    wait_for_done();
    car.HV_current[board.current_step] = get_adc_value(0);

    board.current_step = ((board.current_step+1)%16);
    return;

}

//returns the average of the 16 current values mesured every 100 ms
unsigned int get_average(int value[16]){
    int i;
    float sum = 0;
    unsigned int average;
    for(i=0; i<16; i++){
        sum = sum + value[i];
    }
    average = sum/16;

    return value[0];

}

//returns detections of the fuses and sc signals to be sent in the dcu status
unsigned int check_detections(){
    unsigned int det;

    det = DET_SC_FRONT_BSPD; 
    det = (det <<1 ) | DET_SC_MH_FRONT;
    det = (det <<1 ) | DET_SC_BSPD_BAT;
    det = (det <<1 ) | DET_VCC_AMS;
    det = (det <<1 ) | DET_VCC_FANS_AMS;
    det = (det <<1 ) | DET_VCC_TSAL;	    
    det = (det <<1 ) | DET_VCC_CUA;
    det = (det <<1 ) | DET_SC_ORIGIN;
    det = (det <<1 ) | DET_VCC_EM;
    det = (det <<1 ) | DET_VCC_PUMPS;
    det = (det <<1 ) | DET_VCC_FANS;
    det = (det <<1 ) | DET_VCC_CUB;
    det = (det <<1 ) | DET_VCC_DRS;
    det = (det <<1 ) | DET_VCC_CAN_E;
    det = (det <<1 ) | DET_VCC_BL;

    return det;   
}

//returns the signal values to be sent in the dcu status
unsigned int check_signals(){
    unsigned int sig;

    sig = BUZZ_SIG;
    sig = (sig <<1) | BL_SIG;
    sig = (sig <<1) | DCDC_SWITCH;
    sig = (sig <<1) | PUMP2_SIG;
    sig = (sig <<1) | PUMP1_SIG;
    sig = (sig <<1) | SC_SWITCH;
    sig = (sig <<1) | FAN_SIG;
    sig = (sig <<1) | CUB_SIG;
    sig = (sig <<1) | CUA_SIG;
    //sig = ((sig <<7) | pwm_drs);

    return sig;
}

//gets the temperature values mesured by the board NTC to be sent in the status
unsigned int check_temp(unsigned int pin){

    unsigned int adc_value;
    select_input_pin(pin);
    start_samp();
    wait_for_done();
    adc_value = get_adc_value(0);
    return adc_value;
}

//sets the drs duty cycle depending of something
//to be used and defined
void set_drs_duty_cycle(){
    OC4RS = ((car.DRS_max_value - car.DRS_min_value)/DRS_RANGE)*car.DRS_sig;
    return;
}


//gets bps pressure value from the TE main message and puts it in the car struct
void get_bps_pressure(TE_MESSAGE te_message){
    car.bps_pressure = te_message.BPS_pressure;
    return;
}

void get_apps(TE_MESSAGE te_message){
	car.apps = te_message.APPS;
}


//turn on/off brake light acording to bps_pressure
void set_brake_light(){
    //pressure is more then  1% of max value
    if(car.bps_pressure > BPS_PRESSURE_MAX/100){
        BL_SIG = 1;
    }else{
        BL_SIG = 0;
    }
    return;
}

