#include "config.h"

//define self device id
unsigned int device_id(void){
	return DEVICE_ID_DCU;
}

//trap handler
void trap_handler(TrapType type){

    send_trap_message(type);

    if(0){
        switch(type){
        case HARD_TRAP_OSCILATOR_FAIL:
            break;
        case HARD_TRAP_ADDRESS_ERROR:
            break;
        case HARD_TRAP_STACK_ERROR:
            break;
        case HARD_TRAP_MATH_ERROR:
            break;
        case CUSTOM_TRAP_PARSE_ERROR:
            break;
        default:
            break;
        }
    }
    
    return;
}

//define pins as outputs/inputs
void config_pins(){

    TRIS_NTC1_SIG = 1;        
    TRIS_NTC2_SIG = 1; 
    TRIS_HV_CS = 1;                   
    TRIS_LV_CS = 1;
	TRIS_AN1 = 1;
	TRIS_AN2 = 1;
    TRIS_DET_SC_BSPD_BAT = 1;	 
    TRIS_DET_SC_MH_FRONT = 1;	    
    TRIS_DET_SC_FRONT_BSPD = 1;	    
    TRIS_DET_VCC_BL = 1;		 	    
    TRIS_DET_VCC_CAN_E = 1;	 	    
    TRIS_DET_VCC_DRS = 1;  	 	    		 	    
    TRIS_DET_VCC_CUB = 1;		 	
    TRIS_DET_VCC_FANS = 1;
	
	TRIS_GPIO2 = 0;
    TRIS_BLUE_SIG = 0;
    TRIS_SC_SWITCH = 0;
    TRIS_PUMP1_SIG = 0;

    TRIS_FAN_SIG = 0;
    TRIS_PUMP2_SIG = 0;
    TRIS_DCDC_SWITCH = 0;
    TRIS_DRS_SIG = 0;
    TRIS_BL_SIG = 0;                 
    TRIS_BUZZ_SIG = 0;
    TRIS_DET_VCC_CUA = 1;
    TRIS_DET_VCC_TSAL = 1;
    TRIS_DET_VCC_FANS_AMS = 1;  		    	    
    TRIS_DET_VCC_AMS = 1;

    TRIS_CUA_SIG = 0;
    TRIS_DET_SC_ORIGIN = 1;          
    TRIS_DET_VCC_PUMPS = 1;          
    TRIS_DET_VCC_EM = 1; 	 	   
    TRIS_CUB_SIG = 0;

	TRIS_GPIO1 = 0;

    return;                  
}

//set initial value for the signals 
//check this for new dcu
void set_default_signals(){
    /*logic boards are normally ON*/
    CUA_SIG = 0;
    CUB_SIG = 0;
    /*brake light is normally OFF*/
    BL_SIG = 0;
    /*buzzers are normally OFF*/
    BUZZ_SIG = 0;
    /*fans are normally ON*/
    FAN_SIG = 0;
    /*pumps are normally OFF*/
    PUMP1_SIG = 0;
    PUMP2_SIG = 0;
    
}

//config adc parameters struct 
void config_adc_parameters(ADC_parameters *parameters){
	/* works in idle mode                       
	 * form set to unsigned integer             
	 * auto conversion                          
	 * manual sampling                          
	 * use internal voltage reference           
	 * manualy change between differrent inputs 
	 * no interruption                          
	 * scan pins AN0, AN1, AN2, AN3
	 * do not interrupt                         
	 * turn the module on                       
	 */

    parameters->idle = 1;                           
    parameters->form = 0;                           
    parameters->conversion_trigger = AUTO_CONV;     
    parameters->sampling_type = MANUAL;             
    parameters->voltage_ref = INTERNAL;             
    parameters->sample_pin_select_type = MANUAL;    
    parameters->smpi = 0;                           
    parameters->pin_select = (AN2 | AN3);
    parameters->interrupt_priority = 0;             
    parameters->turn_on = 1;                        
    return;

}

/* config output compare to be used as pwm for the drs sig */
void config_pwm_drs(){
	/* continue to operate in idle mode      
	 * timer 3 is clock source for compare   
	 * pwm mode fault pin disabled           
	 * setting inicial duty-cycle for the min
	 */

    OC4CONbits.OCSIDL = 0;                          
    OC4CONbits.OCTSEL = 1;                          
    OC4CONbits.OCM = 0b0110;                        

    OC4R = DRS_MIN_SIG;                             
    OC4RS = DRS_MIN_SIG+10;                             

    /* initializing with default values */
    car.DRS_max_value = DRS_MAX_SIG;
    car.DRS_min_value = DRS_MIN_SIG;
    car.DRS_sig = DRS_MIN_SIG;

}

/* Select which messages to accept
 */
bool filter_can2(uint16_t sid){
	uint16_t dev_id = CAN_GET_DEV_ID(sid);
	uint16_t msg_id = CAN_GET_MSG_ID(sid);

    if (msg_id == CMD_ID_COMMON_RTD_ON || msg_id == CMD_ID_COMMON_RTD_OFF)
        return true;

    if (dev_id == DEVICE_ID_TE && msg_id == MSG_ID_TE_MAIN)
        return true;

    if (dev_id == DEVICE_ID_INTERFACE) {
		if (msg_id == CMD_ID_INTERFACE_DCU_TOGGLE || 
				msg_id == CMD_ID_INTERFACE_DEBUG_TOGGLE){
            return true;
        }
    }

    return false;
}

